#!/usr/bin/env python
import cv2
import sys
import json
import os
import numpy as np
from numpy.linalg import norm
import matplotlib.pyplot as plt
import seaborn as sns

sys.path.append('../')
from stream import Stream
Stream.BUFFER_SIZE = 35
import preprocess as pre

cv2.namedWindow('frame', flags=cv2.WINDOW_OPENGL)

DATA_DIR = '../test_data/whiteboard-videos/'
files = os.listdir(DATA_DIR)
files = filter(lambda name : name.endswith('.mp4'), files)
files = sorted(files)
num_files = len(files)

file_ratios = []
file_stds = []
file_corner_dists = []
corner_stds = []
file_corner_velocities = []

# file_ratios = [0.9, 0.8, .75, 0.95]
# file_stds = [.09, 0.11, .08, 0.089]
# file_corner_dists = [100, 200, 150, 100]
# corner_stds = [20, 24, 50, 10]
# file_corner_velocities = [100, 20, 10, 9]
print "Stream BUFFER_SIZE: {0}".format(Stream.BUFFER_SIZE)
with open(DATA_DIR + '/rectangles.json') as rectangle_file:
    rectangles = json.load(rectangle_file)
    print rectangles
    print files
    for filename in files:
        short_name = filename.rsplit('.', 1)[0]
        true_rectangle = rectangles[short_name]
        print "Ground-truth rectangle: {0}".format(true_rectangle)
        game_stream = Stream(DATA_DIR + filename)
        print "File name: {0} ({1})".format(filename, short_name)
        ratios = []
        corner_dists = []
        previous_bounding_rectangle = [[0, 0], [0, 0], [0, 0], [0, 0]]
        corner_velocities = []
        counter = 0
        for bounding_rectangle, frame in game_stream:
            # Let the ringbuffer settle
            counter += 1
            if counter < Stream.BUFFER_SIZE:
                continue
            rect_buffer = np.zeros((Stream.FRAME_HEIGHT, Stream.FRAME_WIDTH),
                              dtype=np.uint8)
            rect_array = np.array(bounding_rectangle, dtype=np.int32)
            true_rect_buffer = np.copy(rect_buffer)
            true_rect_array = np.array(true_rectangle, dtype=np.int32)

            cv2.fillConvexPoly(rect_buffer, rect_array, 127)
            cv2.fillConvexPoly(true_rect_buffer, true_rect_array, 127)
            rect_buffer += true_rect_buffer

            _, intersection = cv2.threshold(rect_buffer, 253, 255, cv2.THRESH_BINARY)
            _, union = cv2.threshold(rect_buffer, 126, 255, cv2.THRESH_BINARY)
            intersection_area = np.count_nonzero(intersection > 0)
            union_area = np.count_nonzero(union > 0)
            overlap_ratio = float(intersection_area) / union_area
            ratios.append(overlap_ratio)
            corner_dist = norm(np.array(bounding_rectangle)
                               - np.array(true_rectangle),
                               axis=1)
            corner_dists.append(np.mean(corner_dist))

            pre.draw_connected_points(frame, bounding_rectangle)

            corner_velocities.append(np.absolute(np.array(bounding_rectangle) - np.array(previous_bounding_rectangle)))
            previous_bounding_rectangle = bounding_rectangle
            # frame = game_stream.warp(frame)
            cv2.imshow('frame', frame)
            if cv2.waitKey(1) & 0xFF == ord('q'):
                sys.exit(0)

        average_ratio = np.mean(ratios)
        file_ratios.append(average_ratio)
        std = np.std(ratios)
        file_stds.append(std)

        average_corner_dist = np.mean(corner_dists)
        file_corner_dists.append(average_corner_dist)
        corner_std = np.std(corner_dists)
        corner_stds.append(corner_std)

        corner_velocity = np.mean(np.mean(corner_velocities))
        file_corner_velocities.append(corner_velocity)

        print "Ratio for file {0}: avg {1}, std {2}".format(short_name,
                                                            average_ratio,
                                                            std)
        average_corner_dists = np.mean(corner_dists, 0)
        print "Average corner distances: {0}".format(average_corner_dists)
        print "Overall average corner distance: {0}".format(average_corner_dist)
        print "Corner STD: {0}".format(corner_std)
        print "Corner velocity: {0}".format(corner_velocity)

cv2.destroyAllWindows()

# Seaborn styles
sns.set_style('dark')

# Plot construction
bar_width = 0.5
fig = plt.figure('Rectangle Scores')

ax = fig.add_subplot(311)
ind = np.arange(num_files)
rects1 = ax.bar(ind, file_ratios, bar_width,
                yerr=file_stds,
                error_kw=dict(elinewidth=2, ecolor='red'))

ax.set_xlim(-bar_width/2, len(ind) + bar_width/2)
ax.set_ylim(0, np.max(np.array(file_ratios) + np.array(file_stds)) + 0.1)
ax.set_title('Ratio of Intersection and Union of Bounding Rectangles')
ax.set_ylabel('Rectangle Overlap and Std. Dev.')
xTickMarks = [files[i] for i in xrange(num_files)]
ax.set_xticks(ind + bar_width/2)
xtickNames = ax.set_xticklabels(xTickMarks)
plt.axhline(y=1, linestyle='dotted')

ax = fig.add_subplot(312)
rects2 = ax.bar(ind, file_corner_dists, bar_width,
                yerr=corner_stds,
                error_kw=dict(elinewidth=2, ecolor='red'))
ax.set_xlim(-bar_width/2, len(ind) + bar_width/2)
ax.set_ylim(0, np.max(np.array(file_corner_dists) + np.array(corner_stds)) + 0.1)
ax.set_title('Average Distance from Rectangle Corners to Ground-truth Corners')
ax.set_ylabel('Corner Distance (px) and Std. Dev.')
xTickMarks = [files[i] for i in xrange(num_files)]
ax.set_xticks(ind + bar_width/2)
xtickNames = ax.set_xticklabels(xTickMarks)

ax = fig.add_subplot(313)
rects3 = ax.bar(ind, file_corner_velocities, bar_width)
                #yerr=corner_stds,
                #error_kw=dict(elinewidth=2, ecolor='red'))
ax.set_xlim(-bar_width/2, len(ind) + bar_width/2)
ax.set_ylim(0, np.max(np.array(file_corner_velocities)) + 0.1)
ax.set_title('Average Corner Position Change Between Frames')
ax.set_ylabel('Corner Position Change (px/frame)')
xTickMarks = [files[i] for i in xrange(num_files)]
ax.set_xticks(ind + bar_width/2)
xtickNames = ax.set_xticklabels(xTickMarks)

plt.show()
