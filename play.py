#!/usr/bin/env python
import cv2
import sys
import time

from stream import Stream
import preprocess as pre
from features import get_player, get_distances
from ai import get_direction
from controller import Controller
from videotest import draw_detections, draw_overlay

DEBUG = True

cv2.namedWindow('frame', flags=cv2.WINDOW_OPENGL)

game_stream = Stream(1)  # or filename

control_codes = {-1: Controller.LEFT, 1: Controller.RIGHT}

with Controller('/dev/tty.usbmodem1421') as controller:
    last_output = 0
    control_out = 0
    control_step_size = 3.
    frame_num = 0
    start_time = time.time()
    for bounding_rectangle, frame in game_stream:

        # Play the game

        #pre.draw_connected_points(frame, bounding_rectangle)

        frame = game_stream.warp(frame)
        frame = pre.preprocess(frame)
        if DEBUG:
            frame_pretty = draw_detections(frame)
        player_pos = get_player(frame)
        distances = get_distances(frame, player_pos[0])
        if last_output > 0:
            if last_output - control_step_size <= 1:
                controller.send(control_codes[control_out], Controller.RELEASE)
                last_output = 0
            else:
                #last_output = (last_output + get_direction(player_pos[0], distances))/2.
                last_output -= control_step_size
        elif last_output < 0:
            if last_output + control_step_size >= -1:
                controller.send(control_codes[control_out], Controller.RELEASE)
                last_output = 0
            else:
                #last_output = (last_output + get_direction(player_pos[0], distances))/2.
                last_output += control_step_size

        if last_output == 0:
            control_out = get_direction(player_pos[0], distances)
            print '       ', control_out
            last_output = control_out
            if control_out > 0:
                control_out = 1
            elif control_out < 0:
                control_out = -1
            if control_out != 0:
                controller.send(control_codes[control_out], Controller.PRESS)
        #if control_out != last_output:
        #    if last_output != 0:
        #        controller.send(control_codes[last_output], Controller.RELEASE)
        #    if control_out != 0:
        #        controller.send(control_codes[control_out], Controller.PRESS)
        #    last_output = control_out

        #print frame_num, '   ',
        if DEBUG:
            draw_overlay(frame_pretty, player_pos, 0, distances, control_out*5)
            cv2.putText(frame_pretty, str(frame_num), (0, 400),
                        cv2.FONT_HERSHEY_SIMPLEX, 1, (0.5, 0.5, 0.5))

            # Display the resulting frame
            cv2.imshow('frame', frame_pretty)

        key = cv2.waitKey(1)
        if key & 0xFF == ord('n'):
            controller.send(Controller.UP, Controller.PRESS)
            time.sleep(0.1)
            controller.send(Controller.UP, Controller.RELEASE)
        elif key & 0xFF == ord('q'):
            sys.exit(0)

        frame_num += 1
        #print round(1./(time.time() - start_time), 1)
        start_time = time.time()


cv2.destroyAllWindows()
