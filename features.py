import cv2
import numpy as np
from scipy.stats import circmean

def make_kernel(diam=12, border=8):
    ksize = diam + 2*border
    kernel = -np.ones((ksize, ksize), dtype=float)
    for r in range(border, ksize - border):
        for c in range(border, ksize - border):
            kernel[r][c] = 1
    if np.sum(kernel) != 0:
        kernel /= abs(np.sum(kernel))
    return kernel

PLAYER_KERNEL = make_kernel()
CENTER = (400, 225)

# Helpers to convert to/from polar
def pol2rect(r, theta):
    return(CENTER[0] + r*np.cos(theta), CENTER[1] + r*np.sin(theta))
def rect2pol(x, y, center=CENTER):
    return (np.sqrt((x - center[0])**2 + (y - center[1])**2),
            np.arctan2(y - center[1], x - center[0]))


def get_player(im, thresh=10, crop=100):
    """Given a preprocessed frame, return player position, as rotation angle.
    :reutrns: angle in [0,2pi), radius, confidence in [0,1] (arbitrary scale, 1 is most confident)
              Returns (-1, 0, 0) if detection failed completely."""
    # only bother convolving on the center of the image
    im = im[CENTER[1]-crop:CENTER[1]+crop, CENTER[0]-crop:CENTER[0]+crop]
    im_filt = cv2.filter2D(im, cv2.CV_32F, PLAYER_KERNEL)
    detections = np.where(im_filt > thresh)

    if len(detections) == 0 or len(detections[0]) == 0:
        return -1, 0, 0

    # use median of detections, to reduce noise
    cy = np.median(detections[0])
    cx = np.median(detections[1])

    r, theta = rect2pol(cx, cy, (crop, crop))
    theta = theta % (2*np.pi)

    # confidence is an empirically reasonable function of the spread of the detections
    mean_dist = (np.mean(np.abs(detections[0] - cy)) + np.mean(np.abs(detections[1] - cx)))
    conf = 1 if mean_dist <= 9 else 3./np.sqrt(mean_dist)

    return theta, r, conf

def draw_over_player(im, player_pos):
    theta, r, conf = player_pos
    x, y = pol2rect(r, theta)
    cv2.circle(im, center=(int(x), int(y)), radius=8, color=0, thickness=8)

def get_distances(im, start_angle=0, nbins=24, min_r=80, rot_range=2*np.pi, stride=2):
    """Given a preprocessed frame, raycast from the center to compute the distance to nearest
    obstacles. Returns a list with nbins pairs of polar coordinates: (dist, angle)."""
    angles = np.linspace(start_angle, start_angle + rot_range, nbins + 1)[:-1]
    distances = np.zeros(nbins)
    # for each angle, ray cast until we hit something
    # start at a radius min_r that is sure to be beyond the player and center hexagon
    for i in range(nbins):
        r = min_r
        x, y = pol2rect(r, angles[i])
        while im[int(y), int(x)] == 0:
            r += stride
            x, y = pol2rect(r, angles[i])
            if x < 0 or y < 0 or x >= im.shape[1] or y >= im.shape[0]:
                break
        distances[i] = r
    return np.array(zip(distances, angles))

def estimate_world_rotation(im, resolution=2, angle=360, keep=60):
    """Given an image, returns world rotation in radians (mod pi/3)."""
    dists = get_distances(im, 0, resolution*angle, 0, angle*np.pi/180., 1)[:, 0]
    dists -= np.min(dists)
    #print dists
    max_dist = np.max(dists)
    if max_dist > 20:
        return 0
    weighted_angles = []
    for i in range(len(dists)):
        weighted_angles += [i]*int(dists[i])
    best_angle = circmean(weighted_angles, resolution*keep)
    return best_angle*np.pi/180./resolution
