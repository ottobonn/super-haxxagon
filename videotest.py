#!/usr/bin/env python

import cv2
import numpy as np
import time

from preprocess import preprocess
from features import get_player, get_distances, draw_over_player, pol2rect, PLAYER_KERNEL, estimate_world_rotation
from ai import get_direction

WEBCAM_TEST = True
WEBCAM_CORNERS = [[367, 298], [1217, 70],
                  [522, 722], [1543, 352]]

def toy_rectify(im, corners=WEBCAM_CORNERS):
    pts1 = np.float32(corners)
    pts2 = np.float32([[0, 0], [800, 0], [0, 450], [800, 450]])
    M = cv2.getPerspectiveTransform(pts1, pts2)
    return cv2.warpPerspective(im, M, (800, 450))

def draw_detections(im):
    """Filter image like we do in get_player, for visualization.
    :returns: Color version of im with raw player detection in red."""
    im_filt = cv2.filter2D(im, cv2.CV_32F, PLAYER_KERNEL)
    im_filt[im_filt < 0] = 0
    im_filt[im_filt > 10] = 255
    return np.dstack((im - im_filt, im - im_filt, im))

def draw_overlay(im, player_pos, rot_diff, wall_dists, control_out, outfile=None):
    """Draw player detection and wall rays over the given image."""
    angle, radius, conf = player_pos
    string = '{:.2f},{:.2f}'.format(angle, rot_diff)
    if conf > 0:
        # highlight player
        x, y = pol2rect(radius, angle)
        cv2.circle(im, center=(int(x), int(y)), radius=10, color=(0, 0, 1), thickness=2)

        # draw ai control direction
        if control_out != 0:
            x2, y2 = pol2rect(radius, angle + 0.2*control_out)
            col = (0.5, 0, 0)
            if abs(control_out) >= 3:
                col = (1, 0.5, 0.5)
            cv2.arrowedLine(im, (int(x), int(y)), (int(x2), int(y2)), color=col, thickness=2)

        # draw actual control direction
        if 4 < abs(rot_diff) < 20:
            x3, y3 = pol2rect(radius+10, angle)
            x4, y4 = pol2rect(radius+10, angle + rot_diff/abs(rot_diff))
            col = (0, 0, 1)
            cv2.arrowedLine(im, (int(x3), int(y3)), (int(x4), int(y4)), color=col, thickness=2)

    for i in range(len(wall_dists)):
        dist = wall_dists[i, 0]
        theta = wall_dists[i, 1]
        if dist > 0:
            x1, y1 = pol2rect(80, theta)
            x2, y2 = pol2rect(dist, theta)
            if i == 0 and conf > 0:
                cv2.line(im, (int(x1), int(y1)), (int(x2), int(y2)), color=(0.5, 1, 0.5), thickness=2)
            else:
                cv2.line(im, (int(x1), int(y1)), (int(x2), int(y2)), color=(0, 0.6, 0), thickness=2)
        string += ',{}'.format(int(dist))

    if outfile is not None and conf > 0:
        #print string
        outfile.write(string + '\n')

if __name__ == '__main__':
    if WEBCAM_TEST:
        vc = cv2.VideoCapture('test_data/webcam1-2.mp4')
    else:
        vc = cv2.VideoCapture('test_data/screengrab2.mp4')

    cv2.namedWindow('test', flags=cv2.WINDOW_OPENGL)
    outfile = None

    control_count = 0
    limit = 5

    rotation_parms = [[2, 360, 60],
                      #[2, 360, 72],
                      #[2, 360, 90],
                      [2, 90, 90],
                      [2, 90, 60]
                      ]
    last_player_world_rots = [0]*len(rotation_parms)

    correct = 0
    incorrect = 0

    while True:
        retval, im = vc.read()
        if im is None:
            break
        if WEBCAM_TEST:
            im = toy_rectify(im)
        im = preprocess(im)

        im_color = draw_detections(im)

        player_pos = get_player(im)
        if player_pos[2] > 0:
            draw_over_player(im, player_pos)
            correct += 1
        else:
            incorrect += 1
        print correct, incorrect

        distances = get_distances(im, player_pos[0])
        control_out = get_direction(player_pos[0], distances)
        # implement some control smoothing for the visualization
        if control_out == 0 and control_count > 0:
            control_count -= 1
        elif control_out == 0 and control_count < 0:
            control_count += 1
        elif (control_out < 0 and control_count > -limit) or (control_out > 0 and control_count < limit):
            control_count += control_out

        if player_pos[2] > 0:
            diffs = []
            for i, parms in enumerate(rotation_parms):
                rot = estimate_world_rotation(im, *parms)
                player_world_rot = 180/np.pi*(player_pos[0] - rot) % parms[2]
                diffs.append(((player_world_rot - last_player_world_rots[i] + parms[2]/2) % parms[2]) - parms[2]/2)
                last_player_world_rots[i] = player_world_rot
            if np.any(np.isnan(diffs)):
                rot_diff = 0
            else:
                rot_diff = diffs[np.argmin(np.abs(diffs))]
                print '{:4.1f}  {}{:.1f} ({})'.format(rot*180/np.pi, '.'*abs(int(rot_diff)), rot_diff, diffs)
        else:
            rot_diff = 0

        draw_overlay(im_color, player_pos, rot_diff, distances, control_count, outfile)

        cv2.imshow('test', im_color)
        cv2.waitKey(1)
    if outfile is not None:
        outfile.close()
