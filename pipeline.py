#!/usr/bin/env python

"""Run a simple flask web server to demonstrate the image-processing pipeline.
Visit localhost:5000/pipeline to see some images. See below for other routes."""

import sys
import os
import cv2
from flask import Flask
import jinja2

from preprocess import preprocess

# Image-handling code

def pipeline(frame):
    # TODO add later stages to this pipeline
    return preprocess(frame)

# Pipeline-debugging web app

app = Flask(__name__)
JINJA_ENVIRONMENT = jinja2.Environment(
    loader=jinja2.FileSystemLoader("./templates"),
    extensions=['jinja2.ext.autoescape'],
    autoescape=True)

@app.route("/")
def index():
    template = JINJA_ENVIRONMENT.get_template('index.jinja2')
    return template.render()

@app.route("/pipeline")
def run_pipeline():
    input_dir = './static/samples/'
    output_dir = './static/output/'
    image_tuples = []
    if not os.path.exists(output_dir):
        os.makedirs(output_dir)
    short_image_names = os.listdir(input_dir)
    full_image_names = [input_dir + name for name in short_image_names]
    for index, image_name in enumerate(full_image_names):
        frame = cv2.imread(image_name, cv2.CV_LOAD_IMAGE_COLOR)
        if frame is None:
            print "Warning: couldn't open {0}".format(image_name)
            continue
        output = pipeline(frame)
        output_filename = output_dir + "/" + short_image_names[index]
        image_tuples.append((image_name, output_filename))
        cv2.imwrite(output_filename, output)
    template_values = {
        'title': 'Pipeline Output',
        'image_tuples': image_tuples
    }
    template = JINJA_ENVIRONMENT.get_template('pipeline.jinja2')
    return template.render(template_values)

def main():
    app.run()
    return 0

if __name__ == "__main__":
    sys.exit(main())
