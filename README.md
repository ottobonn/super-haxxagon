# README #


### Summary ###

Bot that plays Super Hexagon on a mobile phone using a webcam video feed. All necessary files, including Bluetooth keyboard spoofer, are included.

Authors
+ Travis Geis
+ Schuyler Smith
+ Rohit Talreja

This project was completed for the Spring 2016 offering of CS 231A at Stanford University


### Setup and Dependencies ###

* Set up
    - Connect webcam and keyboard spoofer module to computer
* Configuration
    - In `play.py`, edit the parameter passed into `Stream` to select which camera to use. On most systems, `0` will use the built-in webcam while `1` will use a USB webcam
* Hardware dependencies
    - Adafruit Bluefruit EZ-Key - 12 Input Bluetooth HID Keyboard Controller
    - Arduino (we used a Micro)
* Software dependencies (Python packages)
    - OpenCV (cv2)
    - Numpy