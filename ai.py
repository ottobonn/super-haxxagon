import numpy as np
import random

PLAYER_SPEED = 1.  # movement speed in revolutions/sec
WALL_SPEED = 500  # closing speed of walls in pixels/sec

def get_direction(player_pos, distances):
    """Given the position of the player (angle) and an array of distances in all directions, return
    the recommended direction of travel: -1, +1, or 0."""

    # assume distances[0] is centered on the player
    if len(distances.shape) > 1:
        distances = distances[:, 0]  # keep distances, ignore angles
    distances = distances.copy()
    distances -= 80

    # Case 1: Everything is far away, do nothing
    N = len(distances)
    factor = WALL_SPEED/PLAYER_SPEED/N  # units pixels per ray
    distances[1:N/2] -= np.arange(1, N/2) * factor
    distances[N/2:] -= np.flipud(np.arange(1, N/2+1)) * factor
    for dist in distances[N/2:]:
        print int(dist),
    print '   ', distances[0], '   ',
    for dist in distances[1:N/2]:
        print int(dist),
    if distances[0] > 80:
        return 0
    else:
        if distances[1] > distances[-1]:
            return 1
        else:
            return -1

    thresh = 0
    right_count = 2
    while distances[right_count] > thresh and right_count < N/2:
        right_count += 1
    left_count = 2
    while distances[-left_count] > thresh and left_count < N/2:
        left_count += 1
    right_max = np.max(distances[1:right_count])
    right_steps = np.argmax(distances[1:right_count]) + 1
    left_max = np.max(distances[-left_count:])
    left_steps = left_count - np.argmax(distances[-left_count:])
    # Case 2: If we'll die soon in one direction, go in the other direction
    if right_count > left_count and left_count < N/4:
        return right_steps
    elif left_count > right_count and right_count < N/4:
        return -left_steps
    print 'c3',
    # Case 3: Go in the direction with the best hole
    if right_max > left_max:
        return right_steps
    else:
        return -left_steps

def random_get_direction(player_pos, distances):
    return random.choice([-1, 0, 1])
