import numpy as np
import matplotlib.pyplot as plt
from skimage.io import imsave
from scipy.ndimage.filters import gaussian_filter1d

from ai import get_direction

PLOT = False
STATS = True

angles = []
movements = []
dists = []

with open('test_data/feats.csv') as feats_file:
    for line in feats_file:
        vals = [float(val) for val in line.split(',')]
        angles.append(vals[0])
        movements.append(vals[1])
        vals = vals[2:]
        #dist = vals[len(vals)/2:] + vals[:len(vals)/2]
        dists.append(np.array(vals))

ai_movements = np.array([get_direction(0, dist_arr) for dist_arr in dists])


angles = np.array(angles)
movements = np.array(movements)*np.pi/180.

movements_filt1 = gaussian_filter1d(movements, 3)

thresh = 0.03
moving = np.abs(movements) > thresh
above = []
below = []
in_range = False
start = 0
for i in range(len(movements)):
    if np.abs(movements_filt1[i]) > thresh and not in_range:
        in_range = True
        start = i
    elif np.abs(movements_filt1[i]) <= thresh and in_range:
        in_range = False
        if movements_filt1[i-1] > thresh:
            above.append((start, i))
        else:
            below.append((start, i))

if STATS:
    true_pos = np.sum(np.logical_and(moving, ai_movements != 0))
    true_neg = np.sum(np.logical_and(np.logical_not(moving), ai_movements == 0))
    false_pos = np.sum(np.logical_and(np.logical_not(moving), ai_movements != 0))
    false_neg = np.sum(np.logical_and(moving, ai_movements == 0))

    print 'Precision:', 1.*true_pos/(true_pos + false_pos)
    print 'Recall:   ', 1.*true_pos/(true_pos + false_neg)
    print 'Accuracy: ', 1.*(true_pos + true_neg)/(true_pos + true_neg + false_pos + false_neg)

if PLOT:
    for start, stop in above:
        plt.axvspan(start, stop, color='y', alpha=0.2)
    for start, stop in below:
        plt.axvspan(start, stop, color='r', alpha=0.1)

    plt.plot(thresh*np.ones(len(movements)), 'k-')
    plt.plot(-thresh*np.ones(len(movements)), 'k-')

    plt.plot(movements, 'b.')
    plt.plot(movements_filt1, 'c-')
    plt.plot(ai_movements, 'g-')
    plt.show()
