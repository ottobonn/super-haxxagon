"""
Control the KeyboardSpoofer.
Requires pyserial.

Usage:

from controller import Controller
with Controller('/dev/ttyUSBN') as controller:
    controller.send(Controller.UP, Controller.PRESS)
    # optionally wait a while...
    controller.send(Controller.UP, Controller.RELEASE)
"""

import serial

class Controller(object):
    UP = 0
    DOWN = 1
    LEFT = 2
    RIGHT = 3

    PRESS = 0
    RELEASE = 1

    def __init__(self, port):
        self._port = serial.Serial(port, timeout=1)

    def __enter__(self):
        return self

    def send(self, key, action):
        self._port.write(str(key))
        self._port.write(str(action))
        self._port.flush()

    def __exit__(self, exc_type, exc_value, traceback):
        self._port.close()
