import cv2
import math
import numpy as np
from numpy.linalg import norm as norm
import random

IMAGE_SIZE = (800, 450)

def to_gray(frame):
    frame = cv2.cvtColor(frame, cv2.COLOR_BGR2HSV)
    return frame[:, :, 2]

def remove_hud(frame):
    """ The hud fills the top 10 percent of the frame"""
    height = frame.shape[1]
    hud_y = np.ceil(height * 0.1)
    return frame[hud_y:, :]

def threshold(frame):
    thresh = np.percentile(frame, 50)*1.8
    thresh = thresh if thresh < 225 else 225
    _, thresholded = cv2.threshold(frame, thresh, 255, cv2.THRESH_BINARY)
    return thresholded

def resize(frame):
    return cv2.resize(frame, IMAGE_SIZE)

def preprocess(frame):
    stages = [resize, to_gray, threshold]
    for stage in stages:
        frame = stage(frame)
    return frame

def find_edges(frame):
    return cv2.Canny(frame, 0, 200)

def find_contours(frame):
    contourInfo = cv2.findContours(frame, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
    return contourInfo


def bounding_rect_corners(rect):
    """
    rect = (x, y, w, h) for a rectangle. (x, y) is the top-left point
    returns the corners of a rectangle of given size [topLeft, topRight, bottomLeft, bottomRight]
    """
    topLeft = [rect[0], rect[1]]
    topRight = [rect[0] + rect[2], rect[1]]
    bottomLeft = [rect[0], rect[1] + rect[3]]
    bottomRight = [rect[0] + rect[2], rect[1] + rect[3]]

    return [topRight, topLeft, bottomLeft, bottomRight]


def reorder_contour_rect_corners(corners):
    """
    corners = the 4 corners of the best fit rectangle to a contour
    returns [topLeft, topRight, bottomLeft, bottomRight] of the best-fit rectangle
        for the contour
    """
    orderedCorners = [None] * 4
    centerX = (reduce(lambda acum, next: acum + next[0], corners) / float(len(corners)))[0]
    centerY = (reduce(lambda acum, next: acum + next[1], corners) / float(len(corners)))[1]

    # assign points to the correct corner
    for c in corners:
        if c[0] <= centerX and c[1] <= centerY:
            orderedCorners[0] = list(c)
        elif c[0] > centerX and c[1] <= centerY:
            orderedCorners[1] = list(c)
        elif c[0] <= centerX and c[1] > centerY:
            orderedCorners[2] = list(c)
        else:
            orderedCorners[3] = list(c)

    return orderedCorners

def ordered_rect_corners(corners):
    """
    Try to order the corners of the contour to [topRight, topLeft,
    bottomLeft, bottomRight].
    """
    corners = np.array(corners)
    centroid = np.mean(corners, axis=0)
    # Find unit-length vectors from the corners to the center
    center_vectors = corners - centroid
    angles = []
    for row, vector in enumerate(center_vectors):
        angles.append(np.degrees(np.arctan2(vector[1], -vector[0])) + 180)
    corner_angles = zip(corners, angles)
    # Sort corners in order of increasing CCW angle from x axis
    corner_angles = sorted(corner_angles, key=lambda pair : pair[1])
    corners = [tuple(pair[0]) for pair in corner_angles]
    return corners

def getAverageRectangle(rectangleBuffer):
    """
    Computes the average bounding rectangle based on the contents of the ring
    buffer.
    """
    averageRect = [[None, None] for i in xrange(4)]
    for i in xrange(4):
        points = [p[i] for p in rectangleBuffer]
        averageRect[i] = [sum(x) / float(len(points)) for x in zip(*points)]
    return averageRect

def find_corners_of_contour(contour, ransac_num_iters=500):
    """
    finds the best-fit bounding rectangle for a contour by randomly selecting points to make
    a quadrilateral and keeping track of the points that produce an internal area closest
    to that of the original contour

    contour = bounding contour of the phone
    ransac_num_iters = number of attempts at selecting the best-fit rectangle
    """
    originalArea = cv2.contourArea(contour)
    bestArea = 0
    bestContour = None
    for i in xrange(ransac_num_iters):
        points = random.sample(xrange(len(contour)), 4)
        newContour = np.concatenate((contour[points[0]], contour[points[1]], contour[points[2]], contour[points[3]]), axis=0)
        newArea = cv2.contourArea(newContour)
        if abs(originalArea - newArea) < abs(originalArea - bestArea):
            bestArea = newArea
            bestContour = newContour
    rect = ordered_rect_corners(bestContour)
    if None in rect:
        return None
    else:
        return rect

def warp_frame(frame, screen_rectangle, desired_screen_size):
    desired_width = desired_screen_size[0]
    desired_height = desired_screen_size[1]
    screen_points = np.float32(screen_rectangle)
    world_points = np.float32(bounding_rect_corners([0, 0, desired_width, desired_height]))
    transformation_matrix = cv2.getPerspectiveTransform(screen_points, world_points)
    frame = cv2.warpPerspective(frame, transformation_matrix, (desired_width, desired_height))
    return frame

def draw_connected_points(frame, points, color=(0, 0, 255), thickness=2):
    count = len(points)
    for i in xrange(count):
        point1 = points[i]
        point1 = (int(point1[0]), int(point1[1]))
        point2 = points[(i + 1) % count]
        point2 = (int(point2[0]), int(point2[1]))
        cv2.line(frame, point1, point2, color, thickness)
    return frame

def draw_line_segments(frame, lines, color=(255, 255, 255), thickness=2):
    for line in lines:
        cv2.line(frame, (line[0], line[1]), (line[2], line[3]), color, thickness)
    return frame

def line_intersection(line1, line2):
    """
    Intersect the two lines given by line1 = (x1, y1, x2, y2) and
    line2 = (x3, y3, x4, y4). Returns (x, y) or None if no intersection.
    """
    x1 = line1[0]
    y1 = line1[1]
    x2 = line1[2]
    y2 = line1[3]
    x3 = line2[0]
    y3 = line2[1]
    x4 = line2[2]
    y4 = line2[3]
    denom = (x1 - x2) * (y3 - y4) - (y1 - y2) * (x3 - x4)
    if denom == 0:
        return None
    px = ((x1*y2-y1*x2)*(x3-x4) - (x1-x2)*(x3*y4-y3*x4)) / denom
    py = ((x1*y2-y1*x2)*(y3-y4) - (y1-y2)*(x3*y4-y3*x4)) / denom
    return (px, py)

def distance_point_to_line(point, line):
    """
    Given point=(x0, y0) and line=(x1, y1, x2, y2), find the L2 distance from the
    point to the line.
    """
    x0 = point[0]
    y0 = point[1]
    x1 = line[0]
    y1 = line[1]
    x2 = line[2]
    y2 = line[3]
    return (abs((y2-y1)*x0 - (x2-x1)*y0 + x2*y1 - y2*x1)
            / np.sqrt((y2 - y1)**2 + (x2 - x1)**2))

def corners_from_lines(lines):
    """
    Find the points of intersection of the neighboring lines in the list "lines".
    For example, given lines [l1, l2, l3, l4], return [p12, p23, p34, p41], where
    p12 is the intersection of l1 and l2; p23 is the intersection of l2 and l3,
    etc.
    """
    corners = []
    num_lines = len(lines)
    for i in xrange(num_lines):
        line1 = lines[i]
        line2 = lines[(i + 1) % num_lines]
        intersection = line_intersection(line1, line2)
        if intersection is None:
            return None
        corners.append(intersection)
    return corners

def angle_from_lines(line1, line2):
    p11 = np.array((line1[0], line1[1]))
    p12 = np.array((line1[2], line1[3]))
    p21 = np.array((line2[0], line2[1]))
    p22 = np.array((line2[2], line2[3]))
    v1 = p12 - p11
    v2 = p22 - p21
    return np.degrees(np.arccos(np.dot(v1, v2) / (norm(v1) * norm(v2))))

def rect_score(candidate_lines, corners, original_area):
    area = cv2.contourArea(np.array(corners))
    horizontal_angle = angle_from_lines(candidate_lines[0], candidate_lines[2])
    vertical_angle = angle_from_lines(candidate_lines[1], candidate_lines[3])
    skewness = (
                angle_from_lines(candidate_lines[0], candidate_lines[1])
                + angle_from_lines(candidate_lines[2], candidate_lines[3])
                ) / 2
    # TODO finalize orthogonality penalty. A highly-skew quad with correct area
    # and parallel sides shouldn't score well! Could use cord length to do it
    normalized_skewness = 90 - skewness / 90
    normalized_area_change = abs((original_area - area) / original_area)
    normalized_angle_mismatch = (360 - horizontal_angle - vertical_angle) / 360
    return normalized_area_change + normalized_angle_mismatch + normalized_skewness

def rect_score2(corners, original_points):
    """
    Given the corners of the rectangle and the list of original contour points,
    find the sum of distances from each point in the original contour to the
    nearest side of the rectangle defined by the corners.
    """
    total_distance = 0
    sides = segments_connecting_points(corners)
    for point in original_points:
        distances = [distance_point_to_line(point, side) for side in sides]
        total_distance += np.mean(distances)
    return total_distance

def rectangle_from_lines(lines, original_contour, frame_bounds, ransac_num_iters=500):
    original_area = cv2.contourArea(original_contour)
    original_points = [row[0] for row in original_contour]
    best_score = np.Infinity
    best_rect = None
    num_lines = len(lines)
    if num_lines < 4:
        return None
    for i in xrange(ransac_num_iters):
        line_indices = random.sample(xrange(num_lines), 4)
        candidate_lines = (lines[line_indices[0]],
                           lines[line_indices[1]],
                           lines[line_indices[2]],
                           lines[line_indices[3]])
        corners = corners_from_lines(candidate_lines)
        if corners is None:
            continue
        #current_score = rect_score(candidate_lines, corners, original_area)
        current_score = rect_score2(corners, original_points)
        if current_score < best_score:
            best_score = current_score
            best_rect = corners
    print best_score
    # Ensure that the selected rectangle is on-screen, and discard it if not
    for corner in best_rect:
        if (corner[0] > frame_bounds[0] or corner[0] < 0
            or corner[1] > frame_bounds[1] or corner[1] < 0):
            return None
    return best_rect

def segments_connecting_points(points):
    """
    Given a list of points, return a list of line
    segments (x1, y1, x2, y2) connecting the adjacent points.
    """
    num_points = len(points)
    lines = []
    for i in xrange(num_points):
        point1 = points[i]
        point2 = points[(i + 1) % num_points]
        lines.append((point1[0], point1[1], point2[0], point2[1]))
    return lines
