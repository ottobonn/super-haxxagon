from controller import Controller
import time

with Controller('/dev/ttyACM0') as controller:
    time.sleep(1)
    while(True):
        for key in xrange(0, 4):
            controller.send(key, Controller.PRESS)
            time.sleep(.1)
            controller.send(key, Controller.RELEASE)
            time.sleep(.1)
