// Emulate a Bluetooth keyboard using an Adafruit Bluefruit EZ-Key

#define LED_PIN 13
#define UP_PIN 2
#define DOWN_PIN 3
#define LEFT_PIN 4
#define RIGHT_PIN 5

typedef enum {
  UP = 0,
  DOWN,
  LEFT,
  RIGHT
} Key;

typedef enum {
  PRESS = 0,
  RELEASE
} Action;

void setup() {
  Serial.begin(9600);
  while(!Serial);
  pinMode(LED_PIN, OUTPUT);
  pinMode(UP_PIN, OUTPUT);
  pinMode(DOWN_PIN, OUTPUT);
  pinMode(LEFT_PIN, OUTPUT);
  pinMode(RIGHT_PIN, OUTPUT);
  digitalWrite(LED_PIN, 0);
  digitalWrite(UP_PIN, RELEASE);
  digitalWrite(DOWN_PIN, RELEASE);
  digitalWrite(LEFT_PIN, RELEASE);
  digitalWrite(RIGHT_PIN, RELEASE);
}

void send(Key k, Action a) {
  int pin;
  int level;
  switch (k) {
    case UP:
      pin = UP_PIN;
      break;
    case DOWN:
      pin = DOWN_PIN;
      break;
    case LEFT:
      pin = LEFT_PIN;
      break;
    case RIGHT:
      pin = RIGHT_PIN;
      break;
    default:
      return;
  }
  switch (a) {
    case PRESS:
      level = 0;
      break;
    case RELEASE:
      level = 1;
      break;
    default:
      return;
  }
  digitalWrite(pin, level);
}

Key parse_key(char key_char) {
  int key_number = key_char - '0';
  return (Key) key_number;
}

Action parse_action(char action_char) {
  int action_number = action_char - '0';
  return (Action) action_number;
}

void loop() {
  Key current_key;
  Action current_action;
  while (Serial.available() < 2);
  char key_char = Serial.read();
  char action_char = Serial.read();
  current_key = parse_key(key_char);
  current_action = parse_action(action_char);
  send(current_key, current_action);
  Serial.println("Sent");
}
