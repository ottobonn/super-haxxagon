#!/usr/bin/env python
import numpy as np
import cv2
import sys

import preprocess as pre

class Stream:
    # dimensions of captured image
    FRAME_WIDTH = 1280
    FRAME_HEIGHT = 720

    # dimensions of output image
    RECTIFIED_SCREEN_WIDTH = 800
    RECTIFIED_SCREEN_HEIGHT = 450
    DESIRED_SCREEN_SIZE = (RECTIFIED_SCREEN_WIDTH, RECTIFIED_SCREEN_HEIGHT)

    # thresholding constants
    BINARY_THRESHOLD = 50
    WHITE_PIXEL_VALUE = 255

    # criteria to search for the contour that best matches a phone screen
    PHONE_TO_FRAME_RATIO_MIN = 0.15
    PHONE_TO_FRAME_RATIO_MAX = 0.4
    DOWNSAMPLING_RATIO = 0.5

    FRAME_AREA = FRAME_WIDTH * FRAME_HEIGHT

    BUFFER_SIZE = 35

    def __init__(self, capture_identifier=0):
        self._cap = cv2.VideoCapture(capture_identifier)
        # set the camera capture parameters
        self._cap.set(3, Stream.FRAME_WIDTH)
        self._cap.set(4, Stream.FRAME_HEIGHT)
        if not self._cap.isOpened():
            raise Exception("Could not open VideoCapture")
        # ring buffer to keep track of the best bounding rectangles for the phone screen
        self._rectangleBuffer = [[[0, 0] for i in xrange(4)]
                                 for j in xrange(Stream.BUFFER_SIZE)]
        self._nextBufferIndex = 0
        self._avg_screen_rectangle = pre.getAverageRectangle(self._rectangleBuffer)

    def __iter__(self):
        return self

    def next(self):
        ret, frame = self._cap.read()
        if not ret:
            raise StopIteration

        grayFrame = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
        _, thresholded = cv2.threshold(grayFrame,
                                       Stream.BINARY_THRESHOLD,
                                       Stream.WHITE_PIXEL_VALUE,
                                       cv2.THRESH_BINARY_INV)
        toContour = np.copy(thresholded)
        contours, hierarchy = pre.find_contours(toContour)
        # contours = sorted(contours, key=lambda c : cv2.contourArea(c),
        #                   reverse=True)

        # phoneContours = filter(lambda c : Stream.PHONE_TO_FRAME_RATIO_MIN
        #                        < cv2.contourArea(c) / Stream.FRAME_AREA
        #                        < Stream.PHONE_TO_FRAME_RATIO_MAX, contours)

        phoneContour = None
        if len(contours) > 0:
            phoneContour = cv2.convexHull(np.concatenate(contours))
            #cv2.drawContours(frame, [phoneContour], -1, (255, 0, 0), 3)
        # for i, contour in enumerate(phoneContours):
        #     # print cv2.contourArea(contour)
        #     phoneContours[i] = cv2.convexHull(contour)

        if phoneContour is not None:
            screenRect = pre.find_corners_of_contour(phoneContour)
            self._rectangleBuffer[self._nextBufferIndex] = screenRect
            self._nextBufferIndex = ((self._nextBufferIndex + 1)
                                     % Stream.BUFFER_SIZE)

        self._avg_screen_rectangle = pre.getAverageRectangle(self._rectangleBuffer)
        return (self._avg_screen_rectangle, frame)

    def warp(self, frame):
        # rectify image based on the best average bounding rect from the last BUFFER_SIZE frames
        screenPoints = np.float32(self._avg_screen_rectangle)
        worldPoints = np.float32(pre.bounding_rect_corners([0, 0, Stream.RECTIFIED_SCREEN_WIDTH, Stream.RECTIFIED_SCREEN_HEIGHT]))
        transformationMatrix = cv2.getPerspectiveTransform(screenPoints, worldPoints)
        frame = cv2.warpPerspective(frame, transformationMatrix, (Stream.RECTIFIED_SCREEN_WIDTH, Stream.RECTIFIED_SCREEN_HEIGHT))
        return frame

    def __exit__(self, exc_type, exc_value, traceback):
        self._cap.release()
